pragma Singleton
import QtQuick 2.5

QtObject {

	function getProfileIndexByName(targetDeviceName) {
		for (var i = 0; i < profiles.length; ++i) {
			if (profiles[i].deviceName == targetDeviceName)
				return i
		}
		return -1
	}

	function getProfileConfigurationByName(targetDeviceName) {
		var index = getProfileIndexByName(targetDeviceName)
		if (index != -1)
			return profiles[index].customConfig
		return null
	}

	readonly property var icons: ["-battery", "misc-fnlock", "misc-backlight", "battery-conservationmode", "battery-rapidcharge", "perf-intelligentcooling", "perf-extremeperformance", "perf-batterysaver"]

	readonly property var profileNames: {
		var names = []
		for (var i = 0; i < profiles.length; ++i) {
			names.push(profiles[i].deviceName)
		}
		return names
	}

	readonly property var profiles: [
		{ // "LENOVO" "82BH" "Yoga 7 14ITL5" "LENOVO_MT_82BH_BU_idea_FM_Yoga 7 14ITL5"
			"deviceName": "Yoga 7 14ITL5",
			"customConfig": {
				"features": [
				{
					"featureName": "Fn lock",
					"categoryName": "Miscellaneous",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.VPC0.HALS",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SALS",
					"acpiTestBit": true,
					"acpiTestBitValue": "10",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "misc-fnlock",
					"states": [
						{ "stateName": "On", "driverValue": "1", "acpiReadValue": "1", "acpiWriteValue": "0xe", "icon": "" },
						{ "stateName": "Off", "driverValue": "0", "acpiReadValue": "0", "acpiWriteValue": "0xf", "icon": "" }
					]
				},
				{
					"featureName": "Conservation mode",
					"categoryName": "Battery",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.VPC0.GBMD",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SBMC",
					"acpiTestBit": true,
					"acpiTestBitValue": "5",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "battery-conservationmode",
					"states": [
						{ "stateName": "On", "driverValue": "1", "acpiReadValue": "1", "acpiWriteValue": "0x03", "icon": "" },
						{ "stateName": "Off", "driverValue": "0", "acpiReadValue": "0", "acpiWriteValue": "0x05", "icon": "" }
					]
				},
				{
					"featureName": "Rapid charge",
					"categoryName": "Battery",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.QCHO",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SBMC",
					"acpiTestBit": false,
					"acpiTestBitValue": "",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "battery-rapidcharge",
					"states": [
						{ "stateName": "On", "driverValue": "", "acpiReadValue": "0x1", "acpiWriteValue": "0x07", "icon": "" },
						{ "stateName": "Off", "driverValue": "", "acpiReadValue": "0x0", "acpiWriteValue": "0x08", "icon": "" }
					]
				},
				{
					"featureName": "Performance mode",
					"categoryName": "Performance",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.SPMO",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.DYTC",
					"acpiTestBit": false,
					"acpiTestBitValue": "",
					"stateToggle": false,
					"singleIcon": true,
					"singleIconValue": "perf-extremeperformance",
					"states": [
						{ "stateName": "Intelligent cooling", "driverValue": "", "acpiReadValue": "0x0", "acpiWriteValue": "0x000FB001", "icon": "" },
						{ "stateName": "Extreme performance", "driverValue": "", "acpiReadValue": "0x1", "acpiWriteValue": "0x0012B001", "icon": "" },
						{ "stateName": "Battery saving", "driverValue": "", "acpiReadValue": "0x2", "acpiWriteValue": "0x0013B001" }
					]
				}
				]
			}
		},
		{ // "LENOVO" "82SD" "IdeaPad 5 14IAL7" "LENOVO_MT_82SD_BU_idea_FM_IdeaPad 5 14IAL7"
			"deviceName": "IdeaPad 5 14IAL7",
			"customConfig": {
				"features": [
				{
					"featureName": "Fn lock",
					"categoryName": "Miscellaneous",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.VPC0.HALS",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SALS",
					"acpiTestBit": true,
					"acpiTestBitValue": "10",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "misc-fnlock",
					"states": [
						{ "stateName": "On", "acpiReadValue": "1", "acpiWriteValue": "0xe", "icon": ""},
						{ "stateName": "Off", "acpiReadValue": "0", "acpiWriteValue": "0xf", "icon": ""}
					]
				},
				{
					"featureName": "Conservation mode",
					"categoryName": "Battery",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.VPC0.GBMD",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SBMC",
					"acpiTestBit": true,
					"acpiTestBitValue": "5",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "battery-conservationmode",
					"states": [
						{ "stateName": "On", "acpiReadValue": "1", "acpiWriteValue": "0x03", "icon": "", "driverValue": "1" },
						{ "stateName": "Off", "acpiReadValue": "0", "acpiWriteValue": "0x05", "icon": "", "driverValue": "0" }
					]
				},
				{
					"featureName": "Rapid charge",
					"categoryName": "Battery",
					"enabled": true,
					"acpiRead": "\\_SB.PC00.LPCB.EC0.BTSG",
					"acpiWrite": "\\_SB.PC00.LPCB.EC0.VPC0.SBMC",
					"acpiTestBit": false,
					"acpiTestBitValue": "",
					"stateToggle": true,
					"singleIcon": true,
					"singleIconValue": "battery-rapidcharge",
					"states": [
						{ "stateName": "On", "acpiReadValue": "0x1", "acpiWriteValue": "0x07", "icon": "" },
						{ "stateName": "Off", "acpiReadValue": "0x0", "acpiWriteValue": "0x08", "icon": "" }
					]
				}
				]
			}
		}
	]
}
