# Ideapad Controls

A KDE Plasmoid for controlling Lenovo Ideapad settings on Linux, using KAuth and the ideapad_acpi driver or acpi_call module.

![screenshot](preview.png)

## Usage
By default, the plasmoid will appear in the system tray, hidden. Through the system tray settings, it can be configured to appear always in the tray or disabled. The plasmoid can also be used as a widget, to be placed on a panel or on the desktop.

## Installation
	git clone https://invent.kde.org/olib/ideapadcontrols.git
    cd ideapadcontrols
    cmake . && sudo make install
    kquitapp5 plasmashell && kstart5 plasmashell &>/dev/null

## Uninstallation
    sudo make uninstall
