#include <QFile>
#include <QTextStream>
#include <QProcess>
#include <QStandardPaths>

#include <KAuth/ActionReply>
#include <KAuth/HelperSupport>

class helper : public QObject {
	Q_OBJECT
public Q_SLOTS:
	KAuth::ActionReply acpiread(const QVariantMap &args);
	KAuth::ActionReply acpiwrite(const QVariantMap &args);
	KAuth::ActionReply dmiread(const QVariantMap &args);
};

KAuth::ActionReply helper::acpiread(const QVariantMap &args) {
	KAuth::ActionReply reply;
	QString object = args["object"].toString();

	if (object.contains(";") || object.contains("\"") || object.contains("\'")) {
		reply = KAuth::ActionReply::HelperErrorReply();
		return reply;
	}

	QProcess *p = new QProcess(this);
	p->start("sh", QStringList() << "-c" << "echo '" + object + "' > /proc/acpi/call && cat /proc/acpi/call");
	p->waitForFinished();
	QString pout(p->readAllStandardOutput());

	if (pout.contains("Error: ")) {
		KAuth::ActionReply::HelperErrorReply();
		reply.setErrorDescription(pout);
		return reply;
	}

	reply.addData("pout", pout);
	return reply;
}

KAuth::ActionReply helper::acpiwrite(const QVariantMap &args) {
	KAuth::ActionReply reply;
	QString object = args["object"].toString();
	QString value = args["value"].toString();

	QProcess *p = new QProcess(this);
	p->start("sh", QStringList() << "-c" << "echo '" + object + " " + value + "' > /proc/acpi/call && cat /proc/acpi/call");
	p->waitForFinished();
	QString pout(p->readAllStandardOutput());

	if (pout.contains("Error: ")) {
		KAuth::ActionReply::HelperErrorReply();
		reply.setErrorDescription(pout);
		return reply;
	}

	return reply;
}

KAuth::ActionReply helper::dmiread(const QVariantMap &args) {
	KAuth::ActionReply reply;

	QProcess *p = new QProcess(this);
	p->start("dmidecode", QStringList() << "--string" << "system-version");
	p->waitForFinished();
	QString pout(p->readAllStandardOutput().trimmed());

	reply.addData("pout", pout);
	return reply;
}

KAUTH_HELPER_MAIN("olib.ideapadcontrols", helper)

#include "helper.moc"
