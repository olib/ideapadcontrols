import QtQuick 2.5
import QtQuick.Layouts 1.3

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents

ColumnLayout {

	property var icon
	default property alias contents: placeholder.children

	RowLayout {

		Image {

			readonly property bool lightMode: PlasmaCore.Theme.textColor.r + PlasmaCore.Theme.textColor.g + PlasmaCore.Theme.textColor.b < 1.5

			Layout.preferredWidth: PlasmaCore.Units.iconSizes.medium
			Layout.preferredHeight: PlasmaCore.Units.iconSizes.medium
			Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

			id: groupImage
			visible: Plasmoid.configuration.showIcons
			source: "../images/" + (lightMode ? icon + "-dark" : icon)
			fillMode: Image.PreserveAspectFit
		}

		ColumnLayout {
			Layout.topMargin: groupImage.visible ? (groupImage.height / 2) - (sampleText.height / 2) : 0
			Layout.bottomMargin: groupImage.visible ? (groupImage.height / 2) - (sampleText.height / 2) : 0

			id: placeholder
		}

		TextMetrics {
			id: sampleText
			text: "AaBbCc"
		}
	}
}
