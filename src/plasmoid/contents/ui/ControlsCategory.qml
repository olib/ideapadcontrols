import QtQuick 2.5
import QtQuick.Layouts 1.3

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents

ColumnLayout {

	property var name
	default property alias contents: placeholder.children

	PlasmaExtras.Heading {
		Layout.fillWidth: true

		text: name
		level: 2
		visible: Plasmoid.configuration.showCategories
	}

	ColumnLayout {
		id: placeholder
	}
}
