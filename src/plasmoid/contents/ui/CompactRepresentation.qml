import QtQuick 2.5

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore

MouseArea {
    anchors.fill: parent
    onClicked: Plasmoid.expanded = !Plasmoid.expanded

    PlasmaCore.IconItem {
        anchors.fill: parent
        source: Plasmoid.icon
    }
}
