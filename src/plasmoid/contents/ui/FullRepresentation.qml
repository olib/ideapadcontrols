import QtQuick 2.5
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.9 as Kirigami
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents

PlasmaExtras.Representation {
	Layout.margins: PlasmaCore.Units.largeSpacing
	Layout.maximumWidth: PlasmaCore.Units.gridUnit * 16
    Layout.maximumHeight: columnLayout.implicitHeight

	ColumnLayout {
		id: columnLayout
		width: parent.width

		Kirigami.InlineMessage {
			Layout.fillWidth: true

			visible: Plasmoid.configuration.profile == "" && !Plasmoid.configuration.useCustomConfig
			text: "The plasmoid needs to be configured."
			icon.source: "help-about-symbolic"
			type: Kirigami.MessageType.Information
			actions: [ Kirigami.Action {
				text: "Configure..."
				icon.name: "configure"
				onTriggered: Plasmoid.action("configure").trigger()
			}]

			// Override Kirigami colors with Plasma theme
			Kirigami.Theme.textColor: PlasmaCore.Theme.textColor
			Kirigami.Theme.negativeTextColor: PlasmaCore.Theme.negativeTextColor
			Kirigami.Theme.backgroundColor: PlasmaCore.Theme.backgroundColor
		}

		ControlsView {}
	}
}
