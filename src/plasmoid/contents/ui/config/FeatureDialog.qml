import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls 1.4 as QQC1
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.9 as Kirigami
import org.kde.plasma.plasmoid 2.0

import "../constants"

Dialog {

	readonly property var acpiAvailable: Plasmoid.nativeInterface.acpiAvailable()

	property var featureIndex
	readonly property var addFeature: featureIndex == null

	property alias featureName: featureNameField.text
	property alias categoryName: categoryNameField.text
	property alias enabled: enabledBox.checked

	property alias acpiRead: acpiReadField.text
	property alias acpiWrite: acpiWriteField.text
	property alias acpiTestBit: acpiTestBitBox.checked
	property alias acpiTestBitValue: acpiTestBitValueField.text

	property alias stateToggle: stateToggleBox.checked
	property alias singleIcon: singleIconBox.checked
	property alias singleIconValueBox: singleIconValueBox

	title: addFeature ? "Add Feature" : "Edit Feature"
	standardButtons: StandardButton.Ok | StandardButton.Cancel

	ColumnLayout {
		anchors.fill: parent

		Kirigami.FormLayout {
			Layout.alignment: Qt.AlignTop
			wideMode: true

			TextField {
				id: featureNameField
				Layout.maximumWidth: implicitWidth
				Kirigami.FormData.label: "Feature name:"
			}

			TextField {
				id: categoryNameField
				Layout.maximumWidth: implicitWidth
				Kirigami.FormData.label: "Category name:"
			}

			CheckBox {
				id: enabledBox
				Kirigami.FormData.label: "Enabled:"
			}

			Kirigami.Separator { Kirigami.FormData.isSection: true }

			TextField {
				id: acpiReadField
				Layout.maximumWidth: implicitWidth
				Kirigami.FormData.label: "Read:"
			}

			RowLayout {

				CheckBox {
					id: acpiTestBitBox
					text: "Test bit:"
					enabled: stateToggleBox.checked
				}

				TextField {
					id: acpiTestBitValueField
					Layout.preferredWidth: Kirigami.Units.gridUnit * 2
					enabled: acpiTestBitBox.checked && acpiTestBitBox.enabled
				}
			}

			Item { Kirigami.FormData.isSection: true }

			TextField {
				id: acpiWriteField
				Layout.maximumWidth: implicitWidth
				Kirigami.FormData.label: "Write:"
			}

			Kirigami.Separator { Kirigami.FormData.isSection: true }

			CheckBox {
				id: stateToggleBox
				Kirigami.FormData.label: "States:"
				text: "Use on/off states"
				onToggled: {
					if (!checked) acpiTestBitBox.checked = false
					if (checked) {
						var onIndex = -1
						var offIndex = -1
						for (var i = 0; i < statesModel.count; ++i) {
							if (statesModel.get(i).stateName == "On")
								onIndex = i
							else if (statesModel.get(i).stateName == "Off")
								offIndex = i
							else {
								statesModel.remove(i--)
							}
						}
						if (onIndex == -1) {
							statesModel.append({
								stateName: "On",
								acpiReadValue: "",
								acpiWriteValue: "",
								icon: ""
							})
							onIndex = statesModel.count - 1
						}
						if (offIndex == -1) {
							statesModel.append({
								stateName: "Off",
								acpiReadValue: "",
								acpiWriteValue: "",
								icon: ""
							})
							offIndex = statesModel.count - 1
						}
						if (onIndex > offIndex) {
							statesModel.move(statesModel.count - 1, 0, 1)
						}
					}
				}
			}

			Label {
				Layout.maximumWidth: labelSizingRow.implicitWidth
				text: "The feature will use a check box instead of radio buttons for each state"
				font: Kirigami.Theme.smallFont
				wrapMode: Text.Wrap
			}

			RowLayout {
				id: labelSizingRow

				CheckBox {
					id: singleIconBox
					text: "Use a single icon"
				}

				ComboBox {
					id: singleIconValueBox
					model: Constants.icons
					enabled: singleIconBox.checked
				}
			}

			Label {
				Layout.maximumWidth: labelSizingRow.implicitWidth
				text: "The feature will use a single icon instead of a changing icon for each state"
				font: Kirigami.Theme.smallFont
				wrapMode: Text.Wrap
			}
		}

		ListModel {
			id: statesModel
		}

		QQC1.TableView {
			Layout.fillWidth: true
			Layout.preferredHeight: Kirigami.Units.gridUnit * 8

			id: statesView
			model: statesModel

			itemDelegate: TextField {
				topPadding: 0 // Fixes top being clipped off
				leftPadding: 8
				verticalAlignment: Text.AlignVCenter // TODO: Doesn't work
				text: styleData.value
				color: styleData.selected && statesView.activeFocus ? "white" : Kirigami.Theme.textColor
				background: null
				enabled: {
					if (styleData.role == "stateName") return !stateToggle
					else return true
				}
				onPressed: {
					statesView.selection.clear()
					statesView.selection.select(styleData.row)
				}
				onTextChanged: statesModel.get(styleData.row)[styleData.role] = text
			}

			Label {
				anchors.centerIn: parent
				text: "Add a state by clicking the \"+\" button"
				visible: statesModel.count === 0
				opacity: enabled ? 1 : 0.3
			}

			QQC1.TableViewColumn {
				role: "stateName"
				title: "State"
				width: Kirigami.Units.gridUnit * 8
			}

			QQC1.TableViewColumn {
				role: "acpiReadValue"
				title: "ACPI read value"
				width: Kirigami.Units.gridUnit * 6
			}

			QQC1.TableViewColumn {
				role: "acpiWriteValue"
				title: "ACPI write value"
				width: Kirigami.Units.gridUnit * 6
			}

			QQC1.TableViewColumn {
				role: "icon"
				title: "Icon"
				width: Kirigami.Units.gridUnit * 6
				delegate: ComboBox {
					model: Constants.icons
					currentIndex: find(styleData.value)
					enabled: !singleIconBox.checked
					onActivated: statesModel.setProperty(styleData.row, "icon", currentText)
				}
			}
		}

		RowLayout {

			Button {
				icon.name: "list-add-symbolic"
				enabled: !stateToggle
				onClicked: {
					statesModel.append({
						stateName: "New state",
						acpiReadValue: "",
						acpiWriteValue: ""
					})
					statesView.selection.clear()
					statesView.selection.select(statesModel.count - 1)
					// TODO: Doesn't work, like in in ConfigGeneral.featureDialog.onAccepted()
				}
			}

			Button {
				icon.name: "list-remove-symbolic"
				enabled: !stateToggle && statesView.currentRow != -1
				onClicked: {
					var row = statesView.currentRow
					statesModel.remove(row)
					if (row == statesModel.count && row != 0) {
						statesView.selection.clear()
						statesView.selction.select(statesModel.count - 1)
					}
				}
			}

			Button {
				icon.name: "go-up-symbolic"
				enabled: !stateToggle && statesView.currentRow > 0
				onClicked: {
					statesModel.move(statesView.currentRow, statesView.currentRow - 1, 1)
					statesView.selection.clear()
					statesView.selection.select(statesView.currentRow - 1)
				}
			}

			Button {
				icon.name: "go-down-symbolic"
				enabled: !stateToggle && statesView.currentRow < statesModel.count - 1 && statesView.currentRow != -1
				onClicked: {
					statesModel.move(statesView.currentRow, statesView.currentRow + 1, 1)
					statesView.selection.clear()
					statesView.selection.select(statesView.currentRow + 1)
				}
			}
		}
	}

	function initialise(feature, index) {
		var feature = feature || {
            featureName: "New feature",
            categoryName: "",
            enabled: true,

			acpiRead: "",
			acpiWrite: "",
			acpiTestBit: false,
			acpiTestBitValue: "",

			stateToggle: true,
			singleIcon: true,
			singleIconValue: "",
			states: null
        }

        featureIndex = index

        featureName = feature.featureName
        categoryName = feature.categoryName
        enabled = feature.enabled

		acpiRead = feature.acpiRead
		acpiWrite = feature.acpiWrite
		acpiTestBit = feature.acpiTestBit
		acpiTestBitValue = feature.acpiTestBitValue

		stateToggle = feature.stateToggle
		singleIcon = feature.singleIcon
		singleIconValueBox.currentIndex = singleIconValueBox.find(feature.singleIconValue)
		statesModel.clear()
		var states = []
		if (feature.states == null) {
			states = [{
				stateName: "On",
				acpiReadValue: "",
				acpiWriteValue: "",
				icon: ""
			}, {
				stateName: "Off",
				acpiReadValue: "",
				acpiWriteValue: "",
				icon: ""
			}]
		} else {
			// States is a model because it was part of a model
			// I have no idea why
			for (var i = 0; i < feature.states.count; ++i) {
				states.push(feature.states.get(i))
			}
		}

		for (var i = 0; i < states.length; ++i) {
			statesModel.append(states[i])
		}
	}

	function getStates() {
		var states = []
		for (var i = 0; i < statesModel.count; ++i) {
			var state = statesModel.get(i)
			states.push({
				stateName: state.stateName,
				acpiReadValue: state.acpiReadValue,
				acpiWriteValue: state.acpiWriteValue,
				icon: state.icon
			})
		}
		return states
	}
}
