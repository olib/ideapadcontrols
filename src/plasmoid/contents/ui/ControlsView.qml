import QtQuick 2.5
import QtQuick.Layouts 1.3

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents

import "constants"

ColumnLayout {

	readonly property var config: {
		if (Plasmoid.configuration.useCustomConfig) return JSON.parse(Plasmoid.configuration.customConfig)
		else return Constants.getProfileConfigurationByName(Plasmoid.configuration.profile)
	}

	// Remove non-unique categories from the model
	readonly property var categoriesModel: config.features.map(feature => feature.categoryName).reduce(function(a,b) { if (a.indexOf(b) < 0) a.push(b); return a }, [])

	Repeater {
		model: categoriesModel

		ControlsCategory {
			name: modelData

			Repeater {
				model: config.features.filter(feature => feature.categoryName == modelData && feature.enabled)

				ControlsGroup {
					property var feature: modelData

					icon: feature.singleIcon ? feature.singleIconValue : states[currentState].icon

					Loader {
						Layout.fillWidth: true

						property var currentState: getCurrentState()

						sourceComponent: feature.stateToggle ? toggleFeature : multistateFeature

						Component {
							id: toggleFeature

							ControlsItem {
								name: feature.featureName

								PlasmaComponents.CheckBox {
									enabled: feature.enabled && currentState != null && Plasmoid.nativeInterface.acpiAvailable()
									onToggled: {
										currentState = (currentState == 0) ? 1 : 0
										setState()
									}
									Component.onCompleted: checked = (currentState == null) ? false : (feature.states[currentState].stateName == "On")
								}
							}
						}

						Component {
							id: multistateFeature

							ColumnLayout {

								Repeater {
									model: modelData.states

									ControlsItem {
										name: modelData.stateName

										PlasmaComponents.RadioButton {
											enabled: feature.enabled && currentState != null && Plasmoid.nativeInterface.acpiAvailable()
											checked: (currentState == null) ? false : currentState == index
											onPressed: {
												currentState = index
												setState()
											}
										}
									}
								}
							}
						}

						function getCurrentState() {
							var value

							if (Plasmoid.nativeInterface.acpiAvailable()) {
								if (modelData.acpiTestBit)
									value = Plasmoid.nativeInterface.acpiReadBit(modelData.acpiRead, modelData.acpiTestBitValue)
								else
									value = Plasmoid.nativeInterface.acpiRead(modelData.acpiRead)
							} else return null

							for (var i = 0; i < modelData.states.length; ++i) {
								if (value == modelData.states[i].acpiReadValue)
									return i
							}

							return null
						}

						function setState() {
							if (Plasmoid.nativeInterface.acpiAvailable())
								Plasmoid.nativeInterface.acpiWrite(modelData.acpiWrite, modelData.states[currentState].acpiWriteValue)
						}
					}
				}
			}
		}
	}
}
