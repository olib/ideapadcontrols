#include "ideapadcontrols.h"

#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QProcess>
#include <KAuth/Action>
#include <KAuth/ExecuteJob>


ideapadcontrols::ideapadcontrols(QObject *parent, const QVariantList &args)
    : Plasma::Applet(parent, args),
      m_acpiPath("/proc/acpi/call")
{
}

ideapadcontrols::~ideapadcontrols()
{
}

bool ideapadcontrols::acpiAvailable() {
	QFile file(m_acpiPath);
	return file.exists();
}

QString ideapadcontrols::acpiRead(QString object) {
	QVariantMap args;
	args["object"] = object;

	KAuth::Action readAction("olib.ideapadcontrols.acpiread");
	readAction.setHelperId("olib.ideapadcontrols");
	readAction.setArguments(args);
	KAuth::ExecuteJob *job = readAction.execute();

	if (!job->exec()) {
		qDebug() << "ideapadcontrols acpiRead error:" << job->error();
		return QString();
	}

	return job->data()["pout"].toString();
}

QString ideapadcontrols::acpiReadBit(QString object, int bit) {
	QString resultStr = acpiRead(object);
	int result = std::stoi(resultStr.toStdString(), 0, 16);

	return QString::number((result>>bit) & 1);
}

void ideapadcontrols::acpiWrite(QString object, QString value) {
	QVariantMap args;
	args["object"] = object;
	args["value"] = value;

	KAuth::Action writeAction("olib.ideapadcontrols.acpiwrite");
	writeAction.setHelperId("olib.ideapadcontrols");
	writeAction.setArguments(args);
	KAuth::ExecuteJob *job = writeAction.execute();

	if (!job->exec())
		qDebug() << "ideapadcontrols acpiWrite error:" << job->error();
}

// TODO: Can we just use infocenter dmidecode helper?
QString ideapadcontrols::systemVersion() {
	KAuth::Action dmiAction("olib.ideapadcontrols.dmiread");
	dmiAction.setHelperId("olib.ideapadcontrols");
	KAuth::ExecuteJob *job = dmiAction.execute();

	if (!job->exec())
		qDebug() << "ideapadcontrols sysver error:" << job->error();

	return job->data()["pout"].toString();
}

K_PLUGIN_CLASS_WITH_JSON(ideapadcontrols, "plasmoid/metadata.json")

#include "ideapadcontrols.moc"
