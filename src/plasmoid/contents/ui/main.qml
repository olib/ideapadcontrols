import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5 as QQC2

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 3.0 as PlasmaComponents

Item {
	Plasmoid.compactRepresentation: CompactRepresentation {}
	Plasmoid.fullRepresentation: FullRepresentation {}
	Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation

	//Plasmoid.switchWidth: PlasmaCore.Units.gridUnit * 16
	//Plasmoid.switchHeight: PlasmaCore.Units.gridUnit * 5.5
	Plasmoid.switchWidth: PlasmaCore.Units.gridUnit * 16
	Plasmoid.switchHeight: PlasmaCore.Units.gridUnit * 3.4

	Plasmoid.status: PlasmaCore.Types.PassiveStatus
}
