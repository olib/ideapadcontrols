add_library(plasma_applet_olib.ideapadcontrols MODULE ideapadcontrols.cpp)
target_link_libraries(plasma_applet_olib.ideapadcontrols
                      Qt5::Gui
                      KF5::Plasma
                      KF5::Auth)
install(TARGETS plasma_applet_olib.ideapadcontrols DESTINATION ${KDE_INSTALL_PLUGINDIR}/plasma/applets)
plasma_install_package(plasmoid olib.ideapadcontrols)

add_executable(helper_ideapadcontrols kauthhelper/helper.cpp)
target_link_libraries(helper_ideapadcontrols
                      KF5::Auth)
install(TARGETS helper_ideapadcontrols DESTINATION ${KAUTH_HELPER_INSTALL_DIR})
kauth_install_helper_files(helper_ideapadcontrols "olib.ideapadcontrols" root)
kauth_install_actions("olib.ideapadcontrols" kauthhelper/olib.ideapadcontrols.actions)
