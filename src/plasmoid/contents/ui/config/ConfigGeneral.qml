import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls 1.4 as QQC1
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.9 as Kirigami
import org.kde.plasma.plasmoid 2.0

import "../constants"

ColumnLayout {

    property alias cfg_showCategories: showCategoriesBox.checked
    property alias cfg_showIcons: showIconsBox.checked
    property alias cfg_profile: profileComboBox.currentText
    property alias cfg_useCustomConfig: customConfigBox.checked
    property string cfg_customConfig

    Component.onCompleted: initialiseCustomConfig()

    Kirigami.InlineMessage {
        Layout.fillWidth: true

        visible: profileComboBox.currentIndex == -1 && !customConfigBox.checked
        text: "The plasmoid needs to be configured."
        icon.source: "help-about-symbolic"
        type: Kirigami.MessageType.Information
        actions: [ Kirigami.Action {
            text: "Detect your device's profile"
            icon.name: "games-hint"
            onTriggered: detectProfile()
        }]
    }

    Kirigami.InlineMessage {
        Layout.fillWidth: true

        visible: !Plasmoid.nativeInterface.acpiAvailable()
        text: "The acpi_call module is not present."
        icon.source: "dialog-error-symbolic"
        type: Kirigami.MessageType.Error
    }

    Kirigami.FormLayout {

        CheckBox {
            id: showCategoriesBox
            Kirigami.FormData.label: "Show:"
            text: "Category headings"
        }

        CheckBox {
            id: showIconsBox
            text: "Icons"
        }

        Kirigami.Separator { Kirigami.FormData.isSection: true }

        ButtonGroup { id: configGroup }

        RowLayout {
            Kirigami.FormData.label: "Configuration:"

            RadioButton {
                text: "Profile:"
                ButtonGroup.group: configGroup
                checked: !cfg_useCustomConfig
            }

            ComboBox {
                //Layout.preferredWidth: implicitWidth
                id: profileComboBox
                model: Constants.profileNames
                enabled: !cfg_useCustomConfig
                Component.onCompleted: currentIndex = find(Plasmoid.configuration.profile)
            }

            Button {
                ToolTip { text: "Detect your device's profile" }
                icon.name: "games-hint"
                enabled: !cfg_useCustomConfig
                onClicked: detectProfile()
            }

            Button {
                ToolTip { text: "Use profile as custom configuration" }
                icon.name: "go-bottom-symbolic"
                enabled: !cfg_useCustomConfig
                onClicked: { useProfileCustomConfigDialog.open() }

                MessageDialog {
                    id: useProfileCustomConfigDialog
                    title: "Ideapad Controls Settings"
                    text: "Are you sure you want to overwrite any existing custom configuration with the currently selected profile's configuration?"
                    icon: StandardIcon.Warning
                    standardButtons: StandardButton.Apply | StandardButton.Cancel
                    onApply: initialiseCustomConfig(Constants.getProfileConfigurationByName(cfg_profile))
                }
            }
        }

        RadioButton {
            id: customConfigBox
            text: "Use custom configuration:"
            ButtonGroup.group: configGroup
        }
    }

    ListModel {
        id: customFeaturesModel
    }

    QQC1.TableView {
        Layout.fillWidth: true
        Layout.preferredHeight: Kirigami.Units.gridUnit * 12

        id: customFeaturesView
        model: customFeaturesModel
        enabled: cfg_useCustomConfig

        itemDelegate: Label {
            leftPadding: 8
            text: styleData.value
            color: styleData.selected && customFeaturesView.activeFocus ? "white" : Kirigami.Theme.textColor
            opacity: enabled ? 1 : 0.3
        }

        onDoubleClicked: openFeatureDialog(currentRow)

        ColumnLayout {
            anchors.centerIn: parent
            spacing: 0
            visible: customFeaturesModel.count === 0
            opacity: enabled ? 1 : 0.3
            Label { text: "Add a feature by clicking the \"+\" button" }
            Label { anchors.horizontalCenter: parent.horizontalCenter
                text: "Edit a feature by double-clicking" }
        }

        QQC1.TableViewColumn {
            role: "featureName"
            title: "Feature"
            width: Kirigami.Units.gridUnit * 16
        }

        QQC1.TableViewColumn {
            role: "categoryName"
            title: "Category"
            width: Kirigami.Units.gridUnit * 10
        }

        QQC1.TableViewColumn {
            role: "enabled"
            title: "Enabled"
            width: Kirigami.Units.gridUnit * 4
            delegate: CheckBox {
                checked: styleData.value
                onToggled: {
                    customFeaturesModel.setProperty(styleData.row, "enabled", checked)
                    customFeaturesView.selection.clear()
                    customFeaturesView.selection.select(styleData.row)
                    saveCustomConfig()
                }
            }
        }
    }

    RowLayout {

        Button {
            icon.name: "list-add-symbolic"
            enabled: cfg_useCustomConfig
            onClicked: openFeatureDialog()
        }

        Button {
            icon.name: "list-remove-symbolic"
            enabled: cfg_useCustomConfig && customFeaturesView.currentRow != -1
            onClicked: {
                var row = customFeaturesView.currentRow
                customFeaturesModel.remove(row)
                if (row == customFeaturesModel.count && row != 0) {
                    customFeaturesView.selection.clear()
                    customFeaturesView.selection.select(customFeaturesModel.count - 1)
                }
                saveCustomConfig()
            }
        }

        Button {
            icon.name: "go-up-symbolic"
            enabled: cfg_useCustomConfig && customFeaturesView.currentRow > 0
            onClicked: {
                customFeaturesModel.move(customFeaturesView.currentRow, customFeaturesView.currentRow - 1, 1)
                customFeaturesView.selection.clear()
                customFeaturesView.selection.select(customFeaturesView.currentRow - 1)
                saveCustomConfig()
            }
        }

        Button {
            icon.name: "go-down-symbolic"
            enabled: cfg_useCustomConfig && customFeaturesView.currentRow < customFeaturesModel.count - 1 && customFeaturesView.currentRow != -1
            onClicked: {
                customFeaturesModel.move(customFeaturesView.currentRow, customFeaturesView.currentRow + 1, 1)
                customFeaturesView.selection.clear()
                customFeaturesView.selection.select(customFeaturesView.currentRow + 1)
                saveCustomConfig()
            }
        }

        Item { Layout.fillWidth: true }

        Button {
            text: "Copy custom configuration"
            icon.name: "edit-copy-symbolic"
            enabled: cfg_useCustomConfig
            TextEdit { id: clipboardHack; visible: false }
            onClicked: {
                clipboardHack.text = JSON.stringify({
                    deviceName: "Your Device Name Here",
                    customConfig: getCustomConfig()
                })
                clipboardHack.selectAll()
                clipboardHack.copy()
            }
        }
    }

    Item { Layout.fillHeight: true }

    FeatureDialog {
        id: featureDialog

        onAccepted: {
            var feature = {
                featureName: featureName,
                categoryName: categoryName,
                enabled: enabled,

                acpiRead: acpiRead,
                acpiWrite: acpiWrite,
                acpiTestBit: acpiTestBit,
                acpiTestBitValue: acpiTestBitValue,

                stateToggle: stateToggle,
                singleIcon: singleIcon,
                singleIconValue: singleIconValueBox.currentValue,
                states: getStates()
            }

            if (addFeature) {
                customFeaturesModel.append(feature)
                customFeaturesView.selection.clear()
                customFeaturesView.selection.select(customFeaturesModel.count - 1) // TODO: Doesn't work?
            } else
                customFeaturesModel.set(featureIndex, feature)

            saveCustomConfig()
        }
    }

    function initialiseCustomConfig(customConfig = null) {
        customConfig = customConfig || JSON.parse(Plasmoid.configuration.customConfig)
        customFeaturesModel.clear()
        var features = customConfig.features
        for (var i = 0; i < features.length; ++i) {
            customFeaturesModel.append(features[i])
        }
        saveCustomConfig()
    }

    function getCustomConfig() {
        var features = []
        for (var i = 0; i < customFeaturesModel.count; ++i) {
            features.push(customFeaturesModel.get(i))
        }
        return {
            features: features
        }
    }

    function saveCustomConfig() {
        cfg_customConfig = JSON.stringify(getCustomConfig())
    }

    function openFeatureDialog(featureIndex = null) {
        featureDialog.initialise(featureIndex == null ? null : customFeaturesModel.get(featureIndex), featureIndex)
        featureDialog.open()
    }

    function detectProfile() {
        profileComboBox.currentIndex = Constants.getProfileIndexByName(Plasmoid.nativeInterface.systemVersion())
    }
}
