#ifndef ideapadcontrols_H
#define ideapadcontrols_H

#include <Plasma/Applet>

class ideapadcontrols : public Plasma::Applet
{
	Q_OBJECT

private:
	QString m_acpiPath;

public:

	ideapadcontrols( QObject *parent, const QVariantList &args );
	~ideapadcontrols();

	Q_INVOKABLE bool acpiAvailable();
	Q_INVOKABLE QString acpiRead(QString);
	Q_INVOKABLE QString acpiReadBit(QString, int);
	Q_INVOKABLE void acpiWrite(QString, QString);
	Q_INVOKABLE QString systemVersion();
};

#endif
